#!/bin/sh
#第一章
# 1.使用; 分割多重指令, 会有移植性问题
#sed 's/MA/Massach;/;s/PA/Penny;/' input
# 1.或在每个命令前放 -e
#sed -e 's/MA/Massach-e/' -e 's/PA/Penny-e/' input

#2.使用-f, 指定script文件, 注意文件里一行就是一条命令; 就算有空格, 都不需要带引号--就是这么炫酷
# 无法重定向要原文件. 我试过, 原文件会变空白 sed -f ./script ./input > ./input
#sed -f ./script ./input

#3. 使用-n阻止行的自动输出, 此时无任何输出
#想只输出匹配行, 还需要在/后面加p, 表示打印
sed -n s/MA/use-n/p ./input

