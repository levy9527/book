使用enter, 结合append或insert创建元素

	d3.selectAll("g").data([1,2,3,4]).enter().append();

使用exit, 结合remove清除多余的元素

	d3.selectAll("g").data([1,2,3,4]).exit().remove();

如元素之前已绑定数据, 重新绑定数据时, 并不会自动刷新视图, 需要手动调用, 如:

	d3.selectAll("g").select("text").text(function(d) {return d});

绑定元素默认是使用数组下标作为key, 可以指定key.  如果没有唯一的key, 可以指定整个对象, 不过使用前需要前stringify一下 

	d3.select("svg").selectAll("circle")
	.data(incomingData, function(d) {
	      return JSON.stringify(d)
			}).
