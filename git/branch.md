# chapter3 分支
![分支示意](http://git.oschina.net/progit/figures/18333fig0305-tn.png)

## [分支概念](http://git.oschina.net/progit/3-Git-%E5%88%86%E6%94%AF.html#3.1-%E4%BD%95%E8%B0%93%E5%88%86%E6%94%AF)
git 创建分支, 其实就是创建一个新的分支指针

> 由于 Git 中的分支实际上仅是一个包含所指对象校验和（40 个字符长度 SHA-1 字串）的文件，所以创建和销毁一个分支就变得非常廉价。说白了，新建一个分支就是向一个文件写入 41 个字节（外加一个换行符）那么简单，当然也就很快了。

git 的 HEAD 指针, 指向你正在工作中的本地分支

每次提交后 HEAD 随着分支一起向前移动

checkout 之后移 HEAD 会动到另一个分支

## [分支的使用场景: master issue hotfix](http://git.oschina.net/progit/3-Git-%E5%88%86%E6%94%AF.html#3.2-%E5%88%86%E6%94%AF%E7%9A%84%E6%96%B0%E5%BB%BA%E4%B8%8E%E5%90%88%E5%B9%B6)

### Fast forward
![Fast forward示例: master merge hoxfix](http://git.oschina.net/progit/figures/18333fig0313-tn.png)

由于当前 master 分支所在的提交对象是要并入的 hotfix 分支的直接上游，Git 只需把 master 分支指针直接右移。换句话说，如果顺着一个分支走下去可以到达另一个分支的话，那么 Git 在合并两者时，只会简单地把指针右移，因为这种单线的历史分支不存在任何需要解决的分歧，所以这种合并过程可以称为快进（Fast forward）。

### 创建了一个包含了合并结果的提交对象
![不能简单地把分支指针右移](http://git.oschina.net/progit/figures/18333fig0317-tn.png)

### 合并冲突

`git status`查阅: 任何包含未解决冲突的文件都会以未合并（unmerged）的状态列出

## [分支管理](http://git.oschina.net/progit/3-Git-%E5%88%86%E6%94%AF.html#3.3-%E5%88%86%E6%94%AF%E7%9A%84%E7%AE%A1%E7%90%86)

`git branch` 列出所有分支

`git branch -v` 查看各个分支最后一个提交对象的信息

`git branch --merge`  查看哪些分支已被并入当前分支, 也就是说哪些分支是当前分支的直接上游

`git branch --no-merged` 查看尚未合并的分支

未合并的分支, 简单地用 git branch -d 删除会提示错误，因为那样做会丢失数据; 不过如果你确实想要删除该分支上的改动，可以用大写的删除选项 -D 强制执行

### [利用分支进行开发的工作流程](http://git.oschina.net/progit/3-Git-%E5%88%86%E6%94%AF.html#3.4-%E5%88%A9%E7%94%A8%E5%88%86%E6%94%AF%E8%BF%9B%E8%A1%8C%E5%BC%80%E5%8F%91%E7%9A%84%E5%B7%A5%E4%BD%9C%E6%B5%81%E7%A8%8B)

### [远程分支 !important](http://git.oschina.net/progit/3-Git-%E5%88%86%E6%94%AF.html#3.5-%E8%BF%9C%E7%A8%8B%E5%88%86%E6%94%AF)

我们用 (远程仓库名)/(分支名) 这样的形式表示远程分支

> 注意远程仓库与与远程分支的区别与联系

你可能有多个远程仓库(比如在Github上fork别人的仓库):

```sh
# git remote -v
origin  https://github.com/levy9527/landscape-plus.git (fetch)
origin  https://github.com/levy9527/landscape-plus.git (push)
xiangming  https://github.com/xiangming/landscape-plus.git (fetch)
xiangming  https://github.com/xiangming/landscape-plus.git (push)
```
其中origin是`git clone`时的远程仓库

#### 分支的指针

![git clone会建立指针本地分支 master 和远程分支 origin/master,它们都指向 origin 上的 master 分支](http://git.oschina.net/progit/figures/18333fig0322-tn.png)

![git fetch 命令会更新本地仓库中 origin/master指针](http://git.oschina.net/progit/figures/18333fig0324-tn.png)

![再添加一个远程仓库, 命名为teamone](http://git.oschina.net/progit/figures/18333fig0325-tn.png)

![git fetch teamone时, 本地仓库多了个teamone/master指针](http://git.oschina.net/progit/figures/18333fig0326-tn.png)

#### 推送本地分支 
语法为`git push remote branch`, 其中远程仓库名remote默认是origin, 分支名branch默认分支是master

如果要把本地serverifx分支推送到origin, 命令为`git push origin serverifx`

也可以运行`git push origin serverifx:serverifx`, 它的意思是“上传我本地的 serverfix 分支到远程仓库中去，仍旧称它为 serverfix 分支”. 通过此语法, 可以实现分支重命名`git push origin serverfix awesomebranch`, 则serverfix在远程仓库中叫做awesomebranch

#### 跟踪远程分支
当你把serverfix分支推送到远程仓库后, 你的协作者fetch origin时, 将得到一个origin/serverfix指针

> 注意, 协作者本地却没有serverfix分支, 也即此时serverfix分支只能由你来编辑

解决办法如下:

1. `git checkout -b serverfix origin/serverfix`
2. `git checkout --track origin/serverfix` Git 1.6.2 版本以上适用

这样你的协作者就可以在本地编辑serverfix分支了

在前面的例子中, 要在本地检出一条追踪xiangming仓库master的分支, 示例命令如下:
`git checkout -b xiangming xiangming/master`

#### 删除远程分支
语法: git push [远程名] :[分支名]. 

比如你要删除origin上的serverfix分支, 可以这样做

`git push origin :serverfix`

它其实是来自git push [远程名] [本地分支]:[远程分支] 语法，如果省略 [本地分支]，那就等于是在说“在这里提取空白然后把它变成[远程分支]”。

##[分支合并](http://git.oschina.net/progit/3-Git-%E5%88%86%E6%94%AF.html#3.6-%E5%88%86%E6%94%AF%E7%9A%84%E8%A1%8D%E5%90%88)

除了git merge, 还可以使用git rebase, 下面比较二者的区别

![开发进程分叉到两个不同分支](http://git.oschina.net/progit/figures/18333fig0327-tn.png)

#### merge
merge会把两个分支最新的快照（C3 和 C4）以及二者最新的共同祖先（C2）进行三方合并，合并的结果是产生一个新的提交对象（C5）

![merge后生成新个提交](http://git.oschina.net/progit/figures/18333fig0328-tn.png)

#### rebase
rebase 会把在 C3 里产生的变化补丁在 C4 的基础上重新打一遍

```sh
# 以下是一种方法
# git checkout experiment
# git rebase master

# 以下是第二种, 会自动切换到experiment分支
git rebase master experiment
```

它的原理是回到两个分支最近的共同祖先，根据当前分支（experiment）后续的历次提交对象（C3），生成一系列文件补丁，然后以基底分支（也即 master）最后一个提交对象（C4）为新的出发点，生成一个新的合并提交对象（C3'），从而改写 experiment 的提交历史，使它成为 master 分支的直接下游

![experiment分支rebase master分支](http://git.oschina.net/progit/figures/18333fig0329-tn.png)


```sh
# 分支开发完毕后, 记得回到 master 分支, 进行合并操作
git checkout master
git merge experiment
```

rebase的好处: 虽然最后整合得到的结果没有任何区别，但衍合能产生一个更为整洁的提交历史, 仿佛所有修改都是在一根线上先后进行的; 而且可以进行一次快进合并, 不会产生新的提交记录

衍合(rebase)应当成一种在推送之前清理提交历史的手段，而且仅仅衍合那些尚未公开的提交对象。如果衍合那些已经公开的提交对象，简单来说就是merge变化并push后, 再用rebase取代之前的merge操作，就会出现叫人沮丧的麻烦. 详情见[Pro Git](http://git.oschina.net/progit/3-Git-%E5%88%86%E6%94%AF.html#3.6-%E5%88%86%E6%94%AF%E7%9A%84%E8%A1%8D%E5%90%88)

> 注:
> 上面是开发过程中, 分支用rebase合并了master的代码. 所以才这么繁琐, 如果直接master通过rebase合并分支的代码, 可以这样写
> `git checkout master`
> `git rebase experiment`

## 再谈代码的提交与合并
`git merge` 后面不带分支名, 则默认是合并origin/master, 需与`git fetch`配合使用(`git pull 默认是这两个命令的组合`)

如果本地master新增提交后, origin也新增提交, 来说说一界面上的不同

- 先fetch, 则会提示`Your branch and 'origin/master' have diverged`, Webstorm中的Version Control界面是不会有变化的

- 直接push, 则会拒绝, 类似下面的信息. 再fetch时, Webstorm中的Version Control界面会得到更新, 出现提交历史的分叉

> To https://git.oschina.net/levy9527/practice.git
> ! [rejected]        master -> master (fetch first)
> error: failed to push some refs to 'https://git.oschina.net/levy9527/practice.git'

注: 这里似乎有点钻牛角尖的味道, 不用太在意

---

`git rebase master local_branch`时, local_branch的变化拼接在master后面, `git log`时看到的最前面的提交记录是local_branch的;
`git pull --rebase`, 则本地master的变化会拼接在origin/master后面, `git log`时看到的最前面的提交记录是本地master的.

![git pull --rebase](http://oc9l6hs7x.bkt.clouddn.com/16-10-24/84113029.jpg)

提交历史虽然干净, 但此时的记录已不是严格按照时间进行排序了

