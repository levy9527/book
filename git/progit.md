[教程来源](http://git.oschina.net/progit/)

## chapter 2
git add 
要暂存这次更新，需要运行 git add 命令（这是个多功能命令，根据目标文件的状态不同，此命令的效果也不同：可以用它开始跟踪新文件，或者把已跟踪的文件放到暂存区，还能用于合并时把有冲突的文件标记为已解决状态等）

.gitignore 的格式规范如下：
所有空行或者以注释符号 ＃ 开头的行都会被 Git 忽略。
可以使用标准的 glob 模式匹配。
  所谓的 glob 模式是指 shell 所使用的简化了的正则表达式。星号（*）匹配零个或多个任意字符；[abc] 匹配任何一个列在方括号中的字符（这个例子要么匹配一个 a，要么匹配一个 b，要么匹配一个 c）；问号（?）只匹配一个任意字符；如果在方括号中使用短划线分隔两个字符，表示所有在这两个字符范围内的都可以匹配（比如 [0-9] 表示匹配所有 0 到 9 的数字）
匹配模式最后跟反斜杠（/）说明要忽略的是目录。
要忽略指定模式以外的文件或目录，可以在模式前加上惊叹号（!）取反。

git diff 查看尚未暂存的文件更新了哪些部分
git diff --cached 查看已经暂存起来的文件和上次提交时的快照之间的差异

git commit
这种方式会启动文本编辑器以便输入本次提交的说明。（默认会启用 shell 的环境变量 $EDITOR 所指定的软件，一般都是 vim 或 emacs。当然也可以按照第一章介绍的方式，使用 git config --global core.editor 命令设定你喜欢的编辑软件。）

git commit 加上 -a 选项，Git 就会自动把所有已经跟踪过的文件暂存起来一并提交，从而跳过 git add 步骤

删除文件
git rm --cached readme.txt //把文件从 Git 仓库中删除（亦即从暂存区域移除），但仍然希望保留在当前工作目录中。
git add xxx && git rm -f xxx //删除之前修改过并且已经放到暂存区域的话，则必须要用强制删除选项 -f,文件也被删除了

移动文件
//前提是 已经commit的file
git mv file_from file_to //此时查看状态信息，可以无误地看到关于重命名操作的说明

查看历史
git log -p -2 //-p 选项展开显示每次提交的内容差异，用 -2 则仅显示最近的两次更新：
或使用 
git show 3ca1e3b7 也可以要查看commit之间的差异
如果git show 不带参数，则查看最近一次commit的内容

根据作者过滤提交记录

	git log --author=levy

撤消操作(针对最近一次的操作)
1.修改最近一次提交 git commit --amend
如果是提交信息写错了，git commit --amend -m 'corret commit msg'
如果是少提交了文件，git add forgotten.file && git commit --amend 
如果是多提交了文件，git rm uselessFile.file && git commit --amend //注意这样会把文件给删除

2.取消已暂存的文件(比如只想add一个文件，不小心add了两个)
git reset HEAD filename

3.取消对文件的修改(如果觉得刚才对文件的修改完全没有必要,回到之前的状态)
git checkout -- filename

> reset HEAD 取消 add/提交
> checkout 取消 修改

//记住，任何已经提交到 Git 的都可以被恢复。即便在已经删除的分支中的提交，或者用 --amend 重新改写的提交，都可以被恢复（关于数据恢复的内容见第九章）。所以，你可能失去的数据，仅限于没有提交过的，对 Git 来说它们就像从未存在过一样。

2.5 远程仓库的使用

1.查看当前的远程库
git remote -v(此为--verbose的简写)

更详细的信息
git remote show remote-name

2.添加远程仓库
git remote add [shortname] [url]

3.推送数据到远程仓库 
git push [remote-name] [local-branch-name] 

默认git push 后面的参数是 origin master, 因为克隆操作会自动使用默认的 master 和 origin 名字
git clone 会自动将远程仓库归于 origin 名下

4.远程仓库的重命名
git remote rename oldname newname

5.远程仓库的删除
git remote rm xxx

2.6 标签
1.新增加
git tag [tag-name] 为当前commit打上轻量级标签

git tag [tag-name] [commit-id] 为以前的commit打标签

2.删除
git tag -d [tag-name]

3.查看
git show [tag-name] 

推送标签
git push origin [tag-name] 或
git push origin --tags 一次性推送多个标签

2.7 技巧

1.别名
```sh
git config --global alias.st status

git config --global alias.cm commit

git config --global alias.br branch

git config --global alias.co checkout
```

如何取消alias? 如上是`--global`选项, 则写入的是~/.gitconfig文件, 把里面的要删除的alias项删除掉就好了.

所以说alias的设置, 其实也可以直接写~/.gitconfig文件的

2.自动补全
建议安装Chrome插件的[git插件](https://chrome.google.com/webstore/detail/github-plus/anlikcnbgdeidpacdbdljnabclhahhmd), 这样可以直接下载git-completion.bash, 而不用下载全部git的源码

[git-completion源码地址](https://github.com/git/git/blob/master/contrib/completion/git-completion.bash)

建议放到`source git-completion` 放到 /etc/bashrc里

