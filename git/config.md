# [配置Git](http://git.oschina.net/progit/7-%E8%87%AA%E5%AE%9A%E4%B9%89-Git.html)

- /etc/gitconfig 文件: 系统中对所有用户都普遍适用的配置。若使用 git config 时用 --system 选项，读写的就是这个文件。
- ~/.gitconfig 文件: 用户目录下的配置文件只适用于该用户。若使用 git config 时用 --global 选项，读写的就是这个文件。
- .git/config 文件: 当前项目的 git 目录中的配置文件。若使用 git config 时用 --local 选项，读写的就是这个文件。这里的配置仅仅针对当前项目有效。

> 每一个级别的配置都会覆盖上层的相同配置，所以 .git/config 里的配置会覆盖 ~/.gitconfig 及 /etc/gitconfig 中的同名变量。

## 必要配置：

	git config --global user.name 'usename'
	git config --global user.eamil 'eamil@qq.com'

## 查看配置:

	git config --list 或 git config -l
	git config user.name 

> 有时候会看到重复的变量名，那就说明它们来自不同的配置文件（比如~/.gitconfig和.git/config），不过最终 Git 实际采用的是最后一个

## 实用配置
### 全局性的.gitignore

	git config --global core.excludesfile ~/.gitignore_global

### 自动修正

	git config --global help.autocorrect 1

假如你在Git中错打了一条命令, 并且在只有一个命令被模糊匹配到的情况下，Git 会自动运行该命令。

> 该配置项只在 Git 1.6.1及以上版本有效, 可用`git --version`查看当前git的版本

### 颜色

	git config --global color.ui true

可选参数:

- true 默认选项
- false 不输出任何颜色
- always 在任何情况下都要着色，即使 Git 命令被重定向到文件或管道, 由Git 1.5.5版本引进. 可以在命令中传`--color`来达到相同目的

颜色还有更详细的配置:

- color.status
- color.branch
- color.diff
- color.interactive

我偏向于直接编辑~/.gitconfig文件, 以 `color.status`为例, 在配置文件中为:

```sh
[color "status"]
  added = green reverse
  changed = yellow
  untracked = dim
```
可以设置的颜色有: normal、black、red、green、yellow、blue、magenta、cyan、white
可以设置的字体有: bold、dim、ul、blink、reverse

### 回车与换行
Windows使用回车和换行(CRLF)两个字符来结束一行，而Mac和Linux只使用换行(LF)一个字符

- Linux和Mac

	git config --global core.autocrlf input

在提交时把CRLF转换成LF，签出时不转换

- Windows

	git config --global core.autocrlf true

提交时自动地把行结束符CRLF转换成LF，而在签出代码时把LF转换成CRLF

> 如果代码开发/运行只在Windows平台, 可以设置`git config --global core.autocrlf false`
