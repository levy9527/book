## preview
This chapter covers
- Organizing your code into modules
- Coding conventions
- Handling one-off events with callbacks
- Handling repeating events with event emitters
- Implementing serial and parallel flow control
- Leveraging flow-control tools

## mark

### exports and modules.exports
Node’s module system, which is based on the CommonJS module specification (www.commonjs.org/specs/modules/1.0/)

If the module is returning more than one function
or variable, the module can specify these by setting the properties of an object called
exports. 

The module.exports mechanism enables you to export a single variable, function, or object.

If you create a module that populates both exports and module.exports, module.exports will be returned and exports will be ignored.

```js
var canadianDollar = 0.91;

function roundTwoDecimals(amount) {
  return Math.round(amount * 100) / 100;
}

// 为exports对象赋予属性, 可以是函数或变量

exports.canadianToUS = function(canadian) {
  return roundTwoDecimals(canadian * canadianDollar);
}

exports.USToCanadian = function(us) {
  return roundTwoDecimals(us / canadianDollar);
}
```
> 个人喜欢这种形式, export出一个对象, 通过obj.method即可调用

```js
var Currency = function(canadianDollar) {
  this.canadianDollar = canadianDollar;
}

Currency.prototype.roundTwoDecimals = function(amount) {
  return Math.round(amount * 100) / 100;
}

Currency.prototype.canadianToUS = function(canadian) {
  return this.roundTwoDecimals(canadian * this.canadianDollar);
}

Currency.prototype.USToCanadian = function(us) {
  return this.roundTwoDecimals(us / this.canadianDollar);
}

// exports = Currency; // Node doesn’t allow exports to be overwritten

module.exports = Currency // 这才是正确形式
```

> 这种形式export是一个函数, 还要func()这样调用一下, 强行OOP, 不喜欢

### require
require is one of the few synchronous I/O operations available in Node. It will block Node from doing anything until the call has finished.

The other thing to be aware of is Node’s ability to cache modules as objects. If two
files in an application require the same module, the first require will store the data
returned in application memory so the second require won’t need to access and evaluate
the module’s source files. The second require will, in fact, have the opportunity
to alter the cached data. This “monkey patching” capability allows one module to
modify the behavior of another, freeing the developer from having to create a new version of it.

## TODO 补上module的查询原理图

## Asynchronous programming techniques
There are two popular models in the Node world for managing response logic: callbacks and event listeners.

- handle one-off events with callbacks
- handle repeating events using event listeners
- Some of the challenges of asynchronous programming

> The Node convention for asynchronous callbacks Most Node built-in modules use callbacks with two arguments: the first argument is for an error, should one occur, and the second argument is for the results. 

### event emitter
Note that you use on (or, alternatively, the longer form addListener) to add a listener to an event emitter:

```js
channel.on('join', function() {
  console.log("Welcome!");
});
```

This join callback, however, won’t ever be called, because you haven’t emitted any
events yet. You could add a line to the listing that would trigger an event using the
emit function:

	channel.emit('join');

> There’s only one special event, called error 

Besides, using the once method can respond to a single event

```js
socket.once('data', function(data) {
  socket.write(data);
});
```

### error handling
If no listener for error is defined when the error event type is emitted, the event emitter will output a stack trace and halt execution. 

This behavior is unique to error type events; when other event types are emitted, and they have no listeners, nothing happens.

The following code shows how an error listener handles an emitted error by logging
into the console:

```js
var events = require('events');
var myEmitter = new events.EventEmitter();

myEmitter.on('error', function(err) {
  console.log('ERROR: ' + err.message);
});

myEmitter.emit('error', new Error('Something is wrong.'));
```

If an error type event is emitted without an error object supplied as the second argument, a stack trace will indicate an “Uncaught, unspecified ‘error’ event” error, and your application will halt

you can define your own response by defining a global handler using the following code:

```js
process.on('uncaughtException', function(err){
  console.error(err.stack);
  process.exit(1);
});
```
