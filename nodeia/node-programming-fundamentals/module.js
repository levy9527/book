// 代码执行前, 会被包起来
//(function (exports, require, module, __filename, __dirname) {
// Your module code actually lives in here
//});

var log = console.log

/*exports 与 module.exports 的关系*/
log('exports === module.exports: ', exports === module.exports)

// 不能像下面这样
// exports = log
// 改变了exports对象, 则再也导不出去了, 因为module.exports 始终为 {}
exports = {}
log('exports === module.exports: ', exports === module.exports)

/*exports 与 module.exports 导出方式的区别*/

// 导出的是个对象, 有个log属性
//exports.log = log 
// 相当于
// module.exports.log = log

// module.exports = log  // 导出的直接是个无名函数
// 下面语句会把前者给覆盖掉
// module.exports = {name: 'obj'}

log(exports)
log(module)
//log(require)
//log(__filename)
//log(__dirname)

