 a middleware component
 is a function that intercepts the request and response objects provided by
 the HTTP server, executes logic, and then either ends the response or passes it to
 the next middleware component.

 To re-emphasize, error handling is a crucial aspect of any kind of application

 You should always include at least one error-handling middleware component in your application by the time it hits production

