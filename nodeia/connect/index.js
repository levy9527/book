var connect = require('connect')
var methodOverride = require('method-override')

var app = connect()
  .use(methodOverride('_method')) 

app.listen(7777, () => {
		console.log(`http://localhost:7777`)
	})

app.use((req, res, next) => {
    console.log(req.url + ' ' + req.method)
    next()
  })

app.use('/', (req, res) => {
  res.setHeader('content-type', 'text/html')

	res.write('<form action="/form" method="post?_method=DELETE" enctype="application/x-www-form-urlencoded">')
	res.write('<input type="hidden" name="_method" value="delete">')
	res.write('<input type="text" name="name">')
	res.write('<button>提交</button>')
	res.write('</form>')
	res.end()
						  
})

