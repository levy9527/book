var https = require('https');
var fs = require('fs');

var options = {
  key: fs.readFileSync('./key.pem'),
  cert: fs.readFileSync('./key-cert.pem')
};

https.createServer(options, function (req, res) {
  console.log('get req')
  res.writeHead(200);
  res.end("hello world\n");
}).listen(5000, function() {
  console.log('server start at https://localhost:5000')
});
