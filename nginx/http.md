# HTTP 配置
## 区块结构
- http
- server
- location
## 模块指令
## 模块变量
### 请求头
格式: $http_headername,中划线要变成下划线
- $http_host 
- $http_user_agent
- $http_x_forwarded_for 如果客户端在代理之后,该变量表示客户端的真实ip地址
### 响应头
格式: $sent_http_headername,中划线要变成下划线 
- $sent_http_content_type 指出被传递资源的MIME类型
- $sent_http_location 指出想要访问的location, 而不是客户端发送请求的location
### nginx变量
- $args 所有结合在一起的字符串查询参数
- $query_string 同上
- $arg_xxx 允许访问查询字符串(GET的参数), xxx替换为具体的参数
- $host 相当于http请求头中的host
- $hostname 服务器的系统名称?
- $remote_addr 客户端ip
- $scheme 返回http/https
- $server_protocol 返回协议及版本号, HTTP/1.0或HTTP/1.1
- $server_addr 每一个变量都需要系统调用,在高流量设置中,可能会影响性能

## location
### 标志(修饰符)
location区段的语法:
`location [=|~|~*|^~|@] pattern {}`

- 无标志(no symbol)
URI的定位必须以指定的pattern开始,不可以使用正则式

- =
URI的定位必须与指定的pattern精确匹配,不可以使用正则

- ~
URI使用正则式区分, 区分大小写(如果操作系统区分大小写的话, Windows不区分!?)

- ~*
URI使用正则式区分, 不区分大小写

- ^~
类似于no symbol行为, 不同的是, 一旦匹配, 停止进行其他匹配

- @ 
定义命名location区段, 客户端不能访问, 只能由内部产生的请求来访问, 如try_files和error_page

### 匹配顺序和优先级
> 配置文件中的书写顺序是不影响匹配优先级的
以下优先级由高到低
1. 带=的location精确匹配
1. no symbol的location精确匹配
1. 带^~的location与请求URI开始匹配
1. 带~或~*的location的正则式匹配
1. no symbol的location与请求URI开始匹配
