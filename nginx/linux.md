# 基础
注意与 OS X 不尽相同

## 用户和组管理
### 用户帐户
- /etc/passwd 系统用户列表
- /etc/shadow 用户密码. Mac不是

为运行Ngnix服务单独新建一个账户, 指定其home目录, 并禁止其shell的访问权限
```sh
useradd -s /sbin/nologin -d /usr/local/nginx nginx
```
- usermod 修改
- userdel 删除
### 组管理
- /etc/group 用户组列表


- groupadd 添加组
- groupmod 修改组
- groupdel 删除组

将一个用户添加新一个新的用户组(保留原有的组)

```sh
usermod --append --groups groupname username
```

## 程序和进程
### 系统服务
一般在/etc/init.d里, 通过`/etc/init.d/name command`来执行. 注意的是init.d目录下的脚本不一定提供下面所有的命令:

- start 启动服务
- stop 通过干净的方式停止服务
- restart 重启服务
- reload 重新加载配置文件
- status 显示服务状态

`service --status-all` 会列出所有系统服务的状态

> 以上知识对Mac不适用

### 进程管理
#### 查找pid
- `ps aux | grep pattern` 其中pattern为正则式
- `top` 列出所有进程的资源占用情况, 默认以CPU占用率排序

#### 杀进程
- `kill -9 pid` 
- `killall pname` 杀掉进程pname产生的所有进程

## 文件系统
Linux文件系统使用修订版的FHS(Filesystem Hierarchy Standard)标准, 该标准规定了

- 软件预定(predict)安装文件和目录的位置
- 用户预定(predict)安装文件和目录的位置

### EXT3 文件系统
相比于Windwos的FAT, FAT32或NTFS, Linux下默认且最受推荐的文件系统是EXT3(遵循FHS)

#### 文件大小
Windows的FAT32单个文件最大为4GB, EXT3则为16T(依赖于块的大小)

EXT3磁盘碎片降到最低, 又不影响性能, 因此不需要对驱动器进行碎片整理

#### 文件名
- 长度为256个字符
- 不需要扩展名, 尽管扩展名有助于使用特定的应用程序打开
- 区分大小写	

#### 节点

#### 文件相关时间
- atime 访问时间
- mtime 修改时间, 仅关系到文件的数据
- ctime 更改时间, 不仅关系到文件数据, 还关系到文件属性

> `touch`可以更新 mtime 与 ctime

#### 链接
- 硬链接 二者相互独立
- 软链接 读写的其实是source file

```sh
# 语法
ln [-s] source target
```
#### 查找文件
- locate 在整个文件系统找出指定文件, 与 updatedb 命令有关
- updatedb 更新文件数据库, 一般通过 corn job 来实现定时更新

在MacBook上第一次执行locate时会出现如下信息:
> WARNING: The locate database (/var/db/locate.database) does not exist.

> To create the database, run the following command:
> 
```sh
  sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.locate.plist
```

> Please be aware that the database can take some time to generate; once the database has been created, this message will no longer appear.

#### 压缩与归档
虽然ZIP和RAR广泛应用于互联网, 但Linux主流选择为.tar.gz或.tar.bz2

压缩

- `tar czvf archive.tar.gz [file1 file2...]`
- `tar cjvf archive.tar.bz2 [file1 file2...]`

> 一般是对一个目录归档

解压

- `tar xzvf archive.tar.gz `
- `tar xjvf archive.tar.bz2 `

如果有ZIP或RAR, 下载Linux版本的工具即可

- `unrar x file.rar`
- `unzip file.zip`

### 目录结构

### 设备
伪设备

- /dev/null
- /dev/full
- /dev/zero
- /dev/random
- /dev/urandom

## 系统管理
使用最少的特权原则:

- 登录不使用root
- 运行特定的服务使用特定的账户

### su 命令
su 是 substitute user的缩写

`su - root ` 连字符表示为继承指定用户的个人设置和环境变量

### sudo 命令
使用指定的账户(默认是root)来执行一个命令, 只需要输入当前账户的密码

相关配置在`/etc/sudoers`文件中, 可以使用命令`visudo`编辑文件

在文件的末尾添加一个新的sudo用户:

```sh
alex ALL=(ALL) ALL
```

### 检查与维护
- 磁盘剩余空间 `df -h `
- 指定目录占用空间(默认当前目录) `du -h`
- 空闲内存 `free`

### 软件包

#### 使用包管理器安装
基于Red Hat系统

```sh
# 示例安装php
yum install php
```

基于Debian系统

```sh
# 示例安装php
apt-get install php
```

#### 下载和手动安装
步骤:

1. 使用wget下载安装包(Red Hat的RPM包或Debian的DEB包)
2. 使用`rpm -ivh` 或 `dpkg -i`安装

#### 从源码安装
步骤:

1. wget下载源码
2. 解压`tar xvf xxx.tar.gz`
3. `cd dir && ./configure`
4. `make` 
5. `sudo make install`

> 记得看readme, 查看具体的选项

### 文件和权限
`ls -l` :

- 第1个字符, 表示文件类型(-表示文件, d表示目录, l表示链接)
- 之后的rwx分别代表 属主(owner)/用户组/其他用户 的权限

目录的权限:

- x 允许可以进入, 例如使用`cd`
- r 允许目录内容可以被列出, 例如使用`ls`
- w 允许在目录中新建或删除文件

### 改变权限

#### 八进制表示法

- 4 表示 x
- 2 表示 w
- 1 表示 r
- 0 没有任何权限

`sudo chmod 777 file`

#### 复杂表示法

```sh
# 语法
sudo chmod who+/-what file
```
- who: u(owner), g(group), o(others), a(所有组合, 默认选项)
- +/-: +表示授予权限, -表示剥夺权限
- what: x, w, r

常用例子:

- `sudo chmod +x script.sh` 脚本可执行
- `sudo chmod go-rwx file` 除属主外, 无人可访问
- `sudo chmod -w file` 连属主也无法编辑



### 改变文件的属主和属组
改变属主

`sudo chown user file`

使用 -R 表示递归, 改变目录

`sudo chown -R user dir`

改变改组

`sudo chgrp group file`

可同时改变属主与属组
`sudo chown user:group file`

