# list some cmd I have never used
- cut
cut -c list [file...]
cut -f [-d delim] [file...]
从输入文件中选择一个或多个字段或一组字符. 默认定界符为tab

	cut -d : -f 6 /etc/passwd

- join
以共同一个键值, 将文件内的记录加以结合

- uniq
sort以后使用, 删除重复记录

- exit
退脚本, 并返回一个退出状态给脚本的调用者. 0 表示成功
如果未提供值, 则以最后一个命令的退出状态为默认的退出状态? 但在命令行下使用exit却默认为0?

- shift
shift类似js数据的shift, 用来处理命令行参数时, 一次向左移一位, 原来的$1会消失,以$2取代, $2变成$3的值,以此类推, $#的值依次减少.

shift 还可以指定参数, 指定一次移动几位:默认为1
