# 第7章 
## read
从标准输入读入一个或多个shell变量

## printf
printf的行为由POSIX标准定义, 因此printf比echo更有可移植性

## 文件描述符处理
文件描述符就是0~9的数字,表示每个进程的打开文件.

0,1,2各自对应标准输入,标准输出,标准错误输出

简单的例子: 将程序的输出传送到一个文件,并将其错误输出到另一个文件

	make 1> result 2> error

因为输出重定向的默认文件描述符是标准输出, 所以上面可以简写为

	make > result 2> error

如果把错误也输出到同一个文件

	make > result 2>&1 
> 2> 表示重定向文件描述符2, 也即标准错误输出
> &1 是shell的语法, 表示文件描述符1所指向的文件
> 2>&1这四个字符是不能有空格的

## 命令替换
命令替换(command substitution)有两种形式

- 使用反引号, 或称重音号将命令包起来

	for i in `cd /old/code; echo *.c`
	do
	  diff -c /old/code/$i $i | more
	done

- 使用$(), 将命令括在里面

	echo outer $(echo inner) 

下面的例子, 使用sed来实现head命令

	# 打印文件前几行
	# 语法: head -N file
	count=$(echo $1 | sed 's/^-//') $ 截去前置的负号
	shift
	sed $(count)q "$@"

