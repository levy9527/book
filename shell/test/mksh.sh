#! /bin/sh
filename=${1:?'filename cannot be empty'}
echo '#! /bin/sh' > ${filename}
chmod +x $filename
echo $@
