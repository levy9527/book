# 查找与替换
## 查找文本

### 查找程序
共有三种程序查找整个文件

- grep 使用POSIX定义的基本正则表达式(BRE)
- egrep 使用扩展的正则表达式(ERE) 会消耗更多的资源
- fgrep 匹配的是固定字符串,而非正则式 匹配速度更快 

> egrep与fgrep被整合到grep了, 如今POSIX标准里只有grep. 只不过大多数UNIX与类UNIX系统中, egrep与fgrep还是可用

	grep -E 即是替代egrep
	grep -F 即是替代fgrep. 

> 使用grep而匹配字符串中不含有正则式元字符时, 默认使用-F选项

### 正则式

- Basic Regular Expression, 简称BRE
- Extended Regular Expression, 简称ERE

### 最长匹配
POSIX标准指出, 完全一致的匹配, 指的是最左边开始匹配, 针对每一个子模式(ERE圆括号里的部分) 由左至右, 必须匹配到最长的可能字符串

了解"从最长的最左边(longest leftmost)"规则非常重要:

	echo Tolstoy is worldly | sed 's/T.*y/Camus/'
	Camus

很明显, 这里只是要匹配Tolstoy, 但由于匹配会扩展到最可能的最长长度的文本量, 所以就一直找到worldly的y了!
你需要定义得更精确些:

	echo Tolstoy is worldly | sed 's/T[[:alpha:]]*y/Camus/'
	Camus is worldly

## 替换文本
### sed
#### 替换细节

- 在结尾指定数字,指示第n个匹配才替换

	sed 's/find/replace/2' < example.txt 仅替代第二个匹配者

- 多个命令

1. 每个编辑命令都使用一个 -e 选项
1. 当命令过多时, 把命令放过一个脚本里, 使用 -f 选项, 脚本每一行一条命令:

	cat fixup.sed
	s/foo/bar/g
	s/chicken/cow/g
	s/draft/post/g

1. 也可以使用分号分隔

	sed 's/foo/bar/g ; s/chicken/cow/g' myfile.xml

> 许多商用sed还不支持此功能, 因此有移植性问题

- 记住脚本里最近一个正则式(同ex与vi) 

	s/foo/bar/3 替换第三个foo为bar
	s//quux/ 替换第一个foo为quux

- 修改定界符

	s:<H3>:</h3>:g	冒号为定界符

#### 注释
注释必须单独一行

> 虽然POSIX指出, 注释可以在脚本里的任何位置, 但很多旧版sed仅允许在首先出现, GNU sed则无此限制
