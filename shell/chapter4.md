# 文本处理
- 排序
sort

- 删除重复
uniq

- 计算行数/字数/字符数
wc [-l | -w | -c]

- 提取前/后几行

head -10 file
tail -10 file 还可以使用 -f 选项, 滚动查看日志
