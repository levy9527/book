# 第6章 变量 判断 循环
## 变量
### 只读与环境
readonly 变量只读

	foo=some
	readonly foo

export 导出变量为环境变量

	bar=another
	export bar

export 是shell内置命令, 可以显示当前环境

	export -p p选项可以没有

env 可以查看/设置环境变量

	env -i PATH=$PATH i选项表示忽略继承值

> env 与 export 在输出环境变量时, 不同在于, env的输出, 环境变量值没有用引号包起来

unset 可以解除环境变量

	unset variable
	unset -f function 删除函数

### 使用变量(参数展开 parameter expansion)
变量引用的方式有:

- $var
- ${var}

警告: 默认情况下, 未定义的变量或无值的变量值为 空字符串. 

	rm -fr /$MYPROGRAM 如果未设置MYPROGRAM, 就会有大灾难发生了!

#### 变量表达式(展开运算符)
- 为变量赋默认值

${var:=value}
${var:-value}

- 变量未定义则显示信息, 并退出当前命令或脚本(不同的shell表现不一样)

${var:?msg}

	${count:?"undefined!"} 将显示count: undefined!

- 测试变量是否有值

${var:+value}

	${count:+1} 如果count存在且有值, 则返回1(也就是"真")

> 上述表达式中, : 是可选的. 如果冒号省略, 则只测试变量是否存在, 而不管它是否值

- 字符串长度

${#var}

	x=word
	echo There are ${#x} chacraters in $x

#### 位置参数(positional parameter)
位置参数指的是shell脚本的命令行参数, 同时也表示shell函数内的函数参数. 从1开始, 大于等于10时, 需要花括号{}括起来

	echo first arg is $1
	echo tenth arg is $10
	file=${1:=/dev/tty} 如果给定参数则使用它, 如无参数则使用/dev/tty

特殊变量:

- $# 传递到shell脚本或函数的参数总数
- $@ $* 表示所有的命令行参数
- "$*" 将所有命令行参数视为单个字符串, 等同于"$1 $2 ..." $IFS(内部字段分隔器)的第一个字符用为分隔符
- "$@" 将所有命令行参数视为单独的个体, 等同于"$1" "$2" ...

POSIX内置shell变量:


#### 算术展开
类似C语言, 不过需要置于$((...))的语法中

## 退出状态
退出状态为0表示成功,也表示真. 内置变量?(通过$?访问)包含了上一次执行的一个程序的退出状态

> 注意, 退出状态而言 0 才表示真, 使用if-else时要注意, 判断命令是否执行成功时, 进行"真"条件的命令退出状态值是0!

可以使用命令 `exit [exit-value]` 退出并返回一个退出状态给脚本的调用者

## 流程控制
### if-else
语法如下

	if pipeline
	then
			some code here
	elif pipeline
	then
			another code here
	else
	fi
> 注意:
> 1. 一个if就对应一个fi, 在嵌套if使用时, 要谨记
> 2. 是elif, 而不是else if

### case
相当于switch, 下面是例子

case $1 in
-f)
  ...
	;;
-d | --directory)  # 使用 or 逻辑
  ...
	;;
*) # 相当于default
  echo $1: unknown option >$2
	exit1
	;; # 非必要
esac # case反过来写

> 注意点:
1. 开始形式为 case $1 in
2. 每匹配值后面带 ) 
3. 每个匹配段落分隔符为 ;;
4. default 匹配为 *)
5. 结束形式为 esac(类似if的fi)

### test
通过退化状态返回其结果, 可以用来测试测试文件,比较字符串/比较数字
语法

- test [ expression ]
- [ [expression] ]

> 注意:第二种形式, 常配合if使用, 方括号两边一定要有空格

### 循环
类似js, 使用的是for in 循环. 循环的对象可以是命令行参数, 文件名或者任何列表(list)形式建立的东西

for in 里的列表(list)是可选的, 如果省略, shell循环会遍历整个命令行参数,就如同你已经输入了 `for i in "$@"`

```sh
for i # 循环命令行参数
do 
  echo $i
done 
```

还有while循环跟until循环

注意util循环, 当条件为true时, 是退出循环

如果在交互式shell里写循环, 应该这样:

	$ while condiction
	$ do
	$ some code 
	$ done

## 函数
函数的定义

```sh
my_func () {
  ...
	return 0
}
```

函数调用, 提供函数名及参数即可

```sh
my_func -v
```

函数里面也可以return, 提前结束函数, 并返回值给调用者

